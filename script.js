var _canvas = document.getElementById('canvas');
var ctx = _canvas.getContext('2d');
var _canvasPC = document.getElementById('cpCanvas');
var ctxPC = _canvasPC.getContext('2d');

var modePC = 0;
var outColor = "red";
var PCshown = false;

var drag = false;


var mode = "pen";
var lw = 20;
var lc = "rgb(225,0,0)";
var ff = "sans-serif"

var oldTextPos = {x:0, y:0};
var textShown = false;
var tempPoint = [];
var components = [];
var components_old = [];

var img;
var img_loaded = false;


function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
};

function mouseMove(evt) {
  var mousePos = getMousePos(_canvas, evt);
  if(mode == "pen" || mode == "eraser"){
    if(mode == "eraser"){
      ctx.globalCompositeOperation="destination-out";
    } else {
      ctx.globalCompositeOperation="source-over";
    }
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke();
    tempPoint.push(mousePos);
  } else if(mode == "triangle"){
    var pt2 = {x: Math.floor((mousePos.x+tempPoint[0].x)/2), y: mousePos.y};
    var pt3 = {x: mousePos.x, y: tempPoint[0].y};
    components.pop();
    components.push({
      type: mode,
      pos: [tempPoint[0], pt2, pt3],
      color: lc,
      width: lw
    });
    redrawall();
    changeMode(mode);
  } else if(mode == "circle"){
    var rd = Math.floor(Math.min(Math.abs(mousePos.x-tempPoint[0].x), Math.abs(mousePos.y-tempPoint[0].y))/2);
    var xoff = (mousePos.x < tempPoint[0].x) ? -1 : 1;
    var yoff = (mousePos.y < tempPoint[0].y) ? -1 : 1;
    var pt = {x: tempPoint[0].x+rd*xoff, y: tempPoint[0].y+rd*yoff}
    components.pop()
    components.push({
      type: mode,
      pos: [pt],
      color: lc,
      width: lw,
      radius: rd
    });
    redrawall();
    changeMode(mode);
  } else if(mode == "rect" || mode == "cut"){
    var pt = {x: Math.min(mousePos.x, tempPoint[0].x), y: Math.min(mousePos.y, tempPoint[0].y)};
    var wd = Math.abs(mousePos.x-tempPoint[0].x);
    var hg = Math.abs(mousePos.y-tempPoint[0].y);
    components.pop();
    if(mode == "rect"){
      components.push({
        type: mode,
        pos: [pt],
        color: lc,
        width: lw,
        w: wd,
        h: hg
      });
    } else {
      components.push({
        type: "select",
        pos: [pt],
        w: wd,
        h: hg
      });
    }
    
    redrawall();
    changeMode(mode);
  }
}

function downClipHandler(evt){
  var mousePos = getMousePos(_canvas, evt);
  var obj = components[components.length-1];
  if(mousePos.x < obj.pos[0].x+obj.offset.x || mousePos.x > obj.pos[0].x+obj.offset.x+obj.w
      || mousePos.y < obj.pos[0].y+obj.offset.y || mousePos.y > obj.pos[0].y+obj.offset.y+obj.h){
    changeMode('cut');
    return;
  }
  drag = true;
  tempPoint.push(mousePos);
}

function moveClipHandler(evt){
  var mousePos = getMousePos(_canvas, evt);
  var obj = components[components.length-1];
  if(mousePos.x < obj.pos[0].x+obj.offset.x || mousePos.x > obj.pos[0].x+obj.offset.x+obj.w
    || mousePos.y < obj.pos[0].y+obj.offset.y || mousePos.y > obj.pos[0].y+obj.offset.y+obj.h){
    _canvas.style.cursor = "default";
  } else {
    _canvas.style.cursor = "move";
  }

  if(drag){
    obj.offset.x += mousePos.x - tempPoint[0].x;
    obj.offset.y += mousePos.y - tempPoint[0].y;
    tempPoint.pop();
    tempPoint.push(mousePos);
    redrawall();
  }
}

function hideTextInput(){
  var inputBox = document.getElementById('textinput');
  inputBox.style.display = 'none';
  textShown = false;
}

function showTextInput(evt){
  var inputBox = document.getElementById('textinput');
  if(!textShown){
    inputBox.value = "";
    inputBox.style.display = 'initial';
    inputBox.style.top = evt.clientY + "px";
    inputBox.style.left = evt.clientX + "px";
    oldTextPos.x = evt.clientX;
    oldTextPos.y = evt.clientY;
    textShown = true;
  } else {
    hideTextInput();
  }
}

function drawText(){
  if(event.key != 'Enter') return;
  initStyles();
  ctx.globalCompositeOperation = "source-over";
  var inputBox = document.getElementById('textinput');
  var rect = _canvas.getBoundingClientRect();
  var textPos = {x: oldTextPos.x - rect.left, y:oldTextPos.y - rect.top};
  ctx.fillText(inputBox.value, textPos.x, textPos.y);
  components.push({
    type: mode,
    pos: textPos,
    text: inputBox.value.slice(),
    color: lc,
    fontface: ff,
    size: lw
  })
  
  hideTextInput();
  update4newComponent();
}

function pushComponents(){
  components.push({
    type: mode,
    pos: tempPoint.slice(),
    color: lc,
    width: lw
  });
}

function redrawall(){
  ctx.clearRect(0, 0, _canvas.width, _canvas.height);
  ctx.globalCompositeOperation="source-over";
  ctx.fillStyle = "white"
  ctx.fillRect(0, 0, _canvas.width, _canvas.height);
  if(img_loaded){
    var xoff = 0, yoff = 0;
    if(img.width < 480){
      xoff = Math.floor((480-img.width)/2);
    }
    if(img.height < 480){
      yoff = Math.floor((480-img.height)/2);
    }
    ctx.drawImage(img, xoff, yoff);
  }
  for(var cmi = 0; cmi < components.length; cmi++){
    switch(components[cmi].type){
      case "pen":
      case "eraser":
      case "triangle":
      case "circle":
      case "rect":
      case "select":
        redrawline(components[cmi]);
        break;
      case "text":
        redrawtext(components[cmi]);
        break;
      case "clip":
        redrawclip(components[cmi]);
        break;
    }
  }
  if(mode == "move"){
    redrawline(components[components.length-1]);
  }
  initStyles();
}

function redrawline(comp){
  if(comp.type == "eraser"){
    ctx.globalCompositeOperation="destination-out";
  } else {
    ctx.globalCompositeOperation="source-over";
  }
  if(comp.type != "select" && comp.type != "clip"){
    ctx.strokeStyle = comp.color;
    ctx.lineWidth = comp.width;
  } else {
    ctx.strokeStyle = "black";
    ctx.lineWidth = 2;
  }
  

  ctx.beginPath();
  if(comp.type == "pen" || comp.type == "eraser"){
    ctx.moveTo(comp.pos[0].x, comp.pos[0].y);
    for(var poi = 1; poi < comp.pos.length; poi++){
      ctx.lineTo(comp.pos[poi].x, comp.pos[poi].y);
    }
    ctx.stroke();
  } else if (comp.type == "triangle"){
    ctx.moveTo(comp.pos[0].x, comp.pos[0].y);
    ctx.lineTo(comp.pos[1].x, comp.pos[1].y);
    ctx.lineTo(comp.pos[2].x, comp.pos[2].y);
    ctx.closePath();
    ctx.stroke();
  } else if (comp.type == "circle"){
    ctx.moveTo(comp.pos[0].x+comp.radius, comp.pos[0].y);
    ctx.arc(comp.pos[0].x, comp.pos[0].y, comp.radius, 0, Math.PI*2);
    ctx.stroke();
  } else if (comp.type == "rect"){
    ctx.rect(comp.pos[0].x, comp.pos[0].y, comp.w, comp.h);
    ctx.stroke();
  } else if (comp.type == "select"){
    ctx.setLineDash([3, 5]);
    ctx.moveTo(comp.pos[0].x, comp.pos[0].y);
    ctx.lineTo(comp.pos[0].x+comp.w, comp.pos[0].y);
    ctx.lineTo(comp.pos[0].x+comp.w, comp.pos[0].y+comp.h);
    ctx.lineTo(comp.pos[0].x, comp.pos[0].y+comp.h);
    ctx.lineTo(comp.pos[0].x, comp.pos[0].y);
    ctx.stroke();
    ctx.setLineDash([]);
  } else if (comp.type == "clip"){
    ctx.setLineDash([3, 5]);
    ctx.moveTo(comp.pos[0].x+comp.offset.x, comp.pos[0].y+comp.offset.y);
    ctx.lineTo(comp.pos[0].x+comp.offset.x+comp.w, comp.pos[0].y+comp.offset.y);
    ctx.lineTo(comp.pos[0].x+comp.offset.x+comp.w, comp.pos[0].y+comp.offset.y+comp.h);
    ctx.lineTo(comp.pos[0].x+comp.offset.x, comp.pos[0].y+comp.offset.y+comp.h);
    ctx.lineTo(comp.pos[0].x+comp.offset.x, comp.pos[0].y+comp.offset.y);
    ctx.stroke();
    ctx.setLineDash([]);
  }
  
  
}

function redrawtext(comp){
  ctx.globalCompositeOperation="source-over";
  ctx.fillStyle = comp.color;
  ctx.font = comp.size+"px "+comp.fontface;
  ctx.fillText(comp.text, comp.pos.x, comp.pos.y);
}

function redrawclip(comp){
  ctx.clearRect(comp.pos[0].x, comp.pos[0].y, comp.w, comp.h);
  ctx.globalCompositeOperation="source-over";
  ctx.drawImage(comp.data, comp.pos[0].x+comp.offset.x, comp.pos[0].y+comp.offset.y);
}

function update4newComponent(){
  $('#t_undo').removeClass("disabled");
  components_old = [];
  $('#t_redo').addClass("disabled");
};

function changeMode(newMode) {
  hideTextInput();
  if(newMode != 'move'){
    if(mode == 'move') $('#t_cut').removeClass("selected");
    else $('#t_'+mode).removeClass("selected");
    mode = newMode;
    $('#t_'+mode).addClass("selected");
    _canvas.removeEventListener('mousemove', moveClipHandler, false);
  } else {
    _canvas.addEventListener('mousemove', moveClipHandler, false);
    mode = newMode;
    
  }
  
  if(mode == "eraser"){
    ctx.globalCompositeOperation="destination-out";
    //ctx.lineCap = 'butt';
  } else {
    ctx.globalCompositeOperation="source-over";
    //ctx.lineCap = 'round';
  }
  
  if(mode == "text"){
    document.getElementById('size').style.display = "inline-block";
    document.getElementById('text_size_icon').style.display = "inline-block";
    document.getElementById('font_family').style.display = "inline-block";
    document.getElementById('brush_size_icon').style.display = "none";
  } else if(mode == "move" || mode == "cut") {
    document.getElementById('size').style.display = "none";
    document.getElementById('font_family').style.display = "none";
  } else {
    document.getElementById('size').style.display = "inline-block";
    document.getElementById('text_size_icon').style.display = "none";
    document.getElementById('font_family').style.display = "none";
    document.getElementById('brush_size_icon').style.display = "inline-block";
  }

  if(mode == "pen"){
    _canvas.style.cursor = "url('./pen.png'), auto";
  } else if(mode == "eraser"){
    _canvas.style.cursor = "url('./eraser.png'), auto";
  } else if(mode == "text"){
    _canvas.style.cursor = "url('./text.png'), auto";
  }  else if(mode == "move"){
    _canvas.style.cursor = "default";
  } else {
    _canvas.style.cursor = "crosshair";
  }

  redrawall();
}

function changeSize(){
  lw = document.getElementById('lineWidth').value;
  ctx.lineWidth = lw;
  ctx.font =  lw+"px "+ff;
}

function changeFontface(){
  var obj = document.getElementById('fontface');
  var selected_index = obj.selectedIndex;
  ff = obj.options[selected_index].value;
  ctx.font =  lw+"px "+ff;
}

function undo(){
  if(mode == 'move')
    changeMode('cut');
  if(components.length <= 0) return;
  components_old.push(components.pop());
  if(components.length <= 0){
    $('#t_undo').addClass("disabled");
  }
  $('#t_redo').removeClass("disabled");
  redrawall();
}

function redo(){
  if(mode == 'move')
    changeMode('cut');
  if(components_old.length <= 0) return;
  components.push(components_old.pop());
  if(components_old.length <= 0){
    $('#t_redo').addClass("disabled");
  }
  $('#t_undo').removeClass("disabled");
  redrawall();
  
}

function reset(clearImg = true, needAlert = true){
  if(needAlert && !confirm("Going to refresh the canvas. Are you sure?")) return false;
  
  clearCanvas();

  initStyles();
  changeMode(mode);

  components = [];
  components_old = [];
  $('#t_undo').addClass('disabled');
  $('#t_redo').addClass('disabled');

  if(clearImg)
    img_loaded = false;
  
  redrawall();
  return true;
}

function clearCanvas(){
  ctx.clearRect(0, 0, _canvas.width, _canvas.height);
  ctx.globalCompositeOperation="source-over";
  ctx.fillStyle = "white"
  ctx.fillRect(0, 0, _canvas.width, _canvas.height);
}

function handleFiles(){
  img = new Image();
  img.onload = drawLoadImg;
  img.onerror = failedLoadImg;
  img.src = URL.createObjectURL(this.files[0]);
}

function drawLoadImg(){
  if(!reset(false)){
    document.getElementById('filebtn').value = null;
    return;
  }
  img_loaded = true;
  var xoff = 0, yoff = 0;
  if(this.width < 480){
    xoff = Math.floor((480-this.width)/2);
  }
  if(this.height < 480){
    yoff = Math.floor((480-this.height)/2);
  }
  ctx.drawImage(this, xoff, yoff);
  document.getElementById('filebtn').value = null;
}

function failedLoadImg(){
  alert('Failed to load a image. Please check the file type.');
  document.getElementById('filebtn').value = null;
}

function initStyles(){
  ctx.strokeStyle = lc;
  ctx.lineWidth = lw;
  ctx.lineCap = 'round';
  ctx.font = lw+"px "+ff;
  ctx.fillStyle = lc;
  ctx.textBaseline = "top";
  ctx.setLineDash([]);

  document.getElementById('colorBox').style["background-color"] = lc;
}

function initApp(){
  initStyles();
  ctx.fillStyle = "white"
  ctx.fillRect(0, 0, _canvas.width, _canvas.height);
  changeMode("pen");
  ctx.fillStyle = lc;
  
  _canvas.addEventListener('mousedown', (evt) => {
  var mousePos = getMousePos(_canvas, evt);
  evt.preventDefault();
  tempPoint = [];
  if(mode != 'text' && mode != 'move'){
    update4newComponent();
    ctx.beginPath();
    ctx.moveTo(mousePos.x, mousePos.y);
    tempPoint.push(mousePos);
    if(mode == "triangle" || mode == "circle" || mode == 'rect' || mode == 'cut'){
      components.push({type: "temp"});
    }
    canvas.addEventListener('mousemove', mouseMove, false);
  } else if (mode == "text"){
      showTextInput(evt);
  } else if (mode == "move"){
      downClipHandler(evt);
  }
});

  _canvas.addEventListener('mouseup', () => {
    drag = false;
    _canvas.removeEventListener('mousemove', mouseMove, false);
    if(mode == "pen" || mode == "eraser"){
      if(tempPoint.length > 1){
        pushComponents();
        tempPoint = [];
      }
    } else if (mode == "cut") {
      var obj = components[components.length-1];
      if(obj.type == "select"){
        changeMode('move');
        obj.type = "clip";
        obj.offset = {x: 0, y: 0};
        obj.w -= 3;
        obj.h -= 3;
        var canvas2=document.createElement("canvas");
        canvas2.width=obj.w;
        canvas2.height=obj.h
        var ctx2=canvas2.getContext("2d");
        ctx2.putImageData(ctx.getImageData(
          obj.pos[0].x+2, obj.pos[0].y+2, 
          obj.w, obj.h), 0, 0);
        obj.data = canvas2;
      }
    }
  }, false);

  _canvas.addEventListener('mouseenter', (evt) => {
    if(evt.buttons != 1){
      _canvas.removeEventListener('mousemove', mouseMove, false);
    } else {
      if(tempPoint.length > 1){
        components.pop();
      }
    }
  });

  _canvas.addEventListener('mouseleave', (evt) => {
    if(evt.buttons == 1){
      if(mode == "pen" || mode == "eraser")
        pushComponents();
    }
  });

  document.getElementById("colorBox").addEventListener('change', ()=>{
    lc = document.getElementById("colorBox").value;
    ctx.strokeStyle = lc;
    ctx.fillStyle = lc;
  })

  document.getElementById('filebtn').addEventListener('change', handleFiles, false);

  document.getElementById('t_download').addEventListener('click', function() {
    this.href = _canvas.toDataURL("image/png");
    this.download = "image.png";
  });
  
  initCP();

  document.getElementById('colorBox').addEventListener('click', ()=>{
    if(PCshown){
      document.getElementById('colorPicker').style.display = "none";
      PCshown = false;
    } else {
      document.getElementById('colorPicker').style.display = "initial";
      PCshown = true;
    }
  })
}

var PCrectW = 55 / Math.cos(2 * Math.PI / 360 * 45);
var PCrectX = (160-PCrectW) /2;

function initCP(){
  var r = 80;
  for (var i = 0; i < 360; i += .1) {
    var rad = i * (2 * Math.PI) / 360,
    angleX = Math.cos(rad),
    angleY = Math.sin(rad),
    lineW = 20;

    ctxPC.strokeStyle = "hsl(" + i + ", 100%, 50%)";
    ctxPC.beginPath();

    ctxPC.moveTo(r + angleX * (r - lineW),r + angleY * (r - lineW));
    ctxPC.lineTo(r + angleX * r,r + angleY * r);
    ctxPC.stroke();
    ctxPC.closePath();
  }

  createRect("red");

  _canvasPC.addEventListener('mousedown', (evt) => {
    var mousePos = getMousePos(_canvasPC, evt);
    evt.preventDefault();
    var dis = Math.sqrt(Math.pow(mousePos.x - 80, 2)+Math.pow(mousePos.y - 80, 2));
    if(dis >= 60 && dis <= 80){
      modePC = 1;
      _canvasPC.addEventListener('mousemove', changeOutColor, false);
      changeOutColor(evt);
      return;
    }
    if(mousePos.x >= PCrectX && mousePos.x <= PCrectX+PCrectW){
      if(mousePos.y >= PCrectX && mousePos.y <= PCrectX+PCrectW){
        modePC = 2;
        _canvasPC.addEventListener('mousemove', changeInColor, false);
        changeInColor(evt);
        return;
      }
    }
  });

  _canvasPC.addEventListener('mouseup', removePCevent, false);

  _canvasPC.addEventListener('mouseleave', removePCevent, false);
}

function getColor(x, y){
  const { data } = ctxPC.getImageData(x, y, 1, 1);
  return 'rgb(' + data[0] + ',' + data[1] + ',' + data[2] + ')';
}

function changeOutColor(evt){
  var mousePos = getMousePos(_canvasPC, evt);
  var dis = Math.sqrt(Math.pow(mousePos.x - 80, 2)+Math.pow(mousePos.y - 80, 2));
  if(dis < 60 || dis > 80){
    return;
  }
  outColor = getColor(mousePos.x, mousePos.y);
  createRect(outColor);
}

function changeInColor(evt){
  var mousePos = getMousePos(_canvasPC, evt);
  if(mousePos.x < PCrectX || mousePos.x > PCrectX+PCrectW){
    return;
  }
  if(mousePos.y < PCrectX || mousePos.y > PCrectX+PCrectW){
    return;
  }
  lc = getColor(mousePos.x, mousePos.y);
  initStyles();
}

function removePCevent(){
  _canvasPC.removeEventListener('mousemove', changeOutColor, false);
  _canvasPC.removeEventListener('mousemove', changeInColor, false);
}

function createRect(color){
  ctxPC.clearRect(PCrectX, PCrectX, PCrectW, PCrectW);
  ctxPC.fillStyle = color
  ctxPC.fillRect(PCrectX, PCrectX, PCrectW, PCrectW);
  let g = ctxPC.createLinearGradient(PCrectX, (PCrectX + PCrectW) / 2, PCrectX + PCrectW, (PCrectX + PCrectW) / 2);
  g.addColorStop(0, "#FFFFFF");
  g.addColorStop(1, "rgba(255,255,255,0)");
  ctxPC.fillStyle = g;
  ctxPC.fillRect(PCrectX, PCrectX, PCrectW, PCrectW);
  g = ctxPC.createLinearGradient(PCrectX, PCrectX + PCrectW, PCrectX, PCrectX)
  g.addColorStop(0, "#000000");
  g.addColorStop(1, "rgba(0,0,0,0)");
  ctxPC.fillStyle = g;
  ctxPC.fillRect(PCrectX, PCrectX, PCrectW, PCrectW);
}



window.onload = initApp;

var cursor_img1 = new Image();
cursor_img1.src = "./pen.png";
var cursor_img2 = new Image();
cursor_img2.src = "./eraser.png";
var cursor_img3 = new Image();
cursor_img3.src = "./text.png";
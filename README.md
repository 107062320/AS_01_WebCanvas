# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Cut                                              | 1~5%      | Y         |


---

### How to use 

#### 介面
![](https://i.imgur.com/k8QWxBK.png)

![](https://i.imgur.com/BfLejT6.png)

    綠色： 工具列表
    粉色： 功能列表
    橘色： 選項列表
    白色： 畫布

#### 工具列表

##### ![](https://i.imgur.com/ggF40El.png) Pen
當被選取時，在畫布上點擊後開始拖曳描繪，放開後停止描繪

##### ![](https://i.imgur.com/sLqXF1y.png) Eraser
當被選取時，在畫布上點擊後開始拖曳清除(至透明)，放開後停止清除
透明處將會顯示半透明棋盤圖樣
![](https://i.imgur.com/bdAMFnj.png)


##### ![](https://i.imgur.com/pnLroID.png) Text
當被選取時，在畫布上點擊後出現輸入框，再次於畫布其他處點擊後消失
輸入完畢後按下enter鍵，將在畫布指定位置呈現

##### ![](https://i.imgur.com/KXgrPHS.png) Triangle
當被選取時，在畫布上點擊後拖曳產生三角形，放開後確定大小
此三角形方向之上下決定自起始位置與終點位置的關係
前者較高時為下，反之則為上
寬和高對應兩位置的x和y差距

##### ![](https://i.imgur.com/MVdqmPE.png) Circle
當被選取時，在畫布上點擊後拖曳產生圓形，放開後確定大小
直徑決定於起始位置與終點位置的x和y的差距中較小者
圓心決定於起始位置的x, y各加半徑後的位置

##### ![](https://i.imgur.com/9hRHX1m.png) Rectangle
當被選取時，在畫布上點擊後拖曳產生長方形，放開後確定大小
左上及右下座標取自起始位置與終點位置中最小及最大的x, y
以此產生相對應的長方形

##### ![](https://i.imgur.com/rXosvJn.png) Cut (Experimental)
當被選取時，在畫布上點擊後拖曳產生長方形範圍，放開後確定
再點擊此範圍可任意拖曳
若點擊其他畫布範圍，可再此選取其他範圍

還原時僅會還原至原本未拖曳前的狀態，並不會記錄移動過程
![](https://i.imgur.com/W38E3Ui.png)



#### 功能列表

##### ![](https://i.imgur.com/lhtZHQc.png) Undo
還原上一步
若曾 Refresh 或 Upload，**此功能無法還原**

##### ![](https://i.imgur.com/FikFtoJ.png) Redo
重做上一步
若使用過工具列表的任一項工具作畫過後，**此功能無法重做**

##### ![](https://i.imgur.com/EbJ3Moc.png) Refresh
將畫布全部塗成全白
此功能使用後無法使用還原功能回復

使用前需確認

##### ![](https://i.imgur.com/U1G51B6.png) Upload
上傳圖片檔至畫布
畫布將被清空並將指定圖片置中載入
若圖片的寬高超過畫布的寬高(480px * 480px)
超過的尺度將會改由貼邊處理
(即寬超過將改為靠左，高超過改為靠上)

若載入失敗會跳出警告提醒
使用前需確認

##### ![](https://i.imgur.com/33n4ip9.png) Download
下載當前畫布為圖片檔

##### ![](https://i.imgur.com/zOotDv5.png) Color Picker
點擊後可挑選顏色
並將目前挑選的顏色顯示於此
![](https://i.imgur.com/mlT48jB.png)


#### 選項列表

##### ![](https://i.imgur.com/aF14KqZ.png) 筆刷寬度
適用於 Pen/Eraser/Triangle/Circle/Rectangle
##### ![](https://i.imgur.com/jqKNSmg.png) 文字寬度
適用於 Text
##### ![](https://i.imgur.com/UckAnYg.png) 文字字體
適用於 Text



### Bonus Function description

#### 實作Cut

Cut的實做主要分成兩個部分：Select和Move

#### Select
選取範圍的時候，採用和Rectangle類似的方法
點擊畫布時將當前的滑鼠座標記錄下來
並在拖曳時根據新的滑鼠點生成相對應的長方形
讓使用者得知目前的範圍為何

為了提高使用者關注度，將選取用的長方形設為虛線
使用到下列屬性：
```javascript=
ctx.setLineDash([3, 5]);
```
setLineDash可傳入一Array，依序代表實線/空白/實線...的長度
並以此重複

在Select狀態放開滑鼠後，將當前的select物件轉換成clip物件
將範圍稍微內縮避免擷取到顯示範圍用的長方形
並新創一個canvas儲存當前的範圍的ImageData
以供後面重繪使用
```javascript=
var obj = components[components.length-1];

if(obj.type == "select"){

    changeMode('move');
    
    obj.type = "clip";
    obj.offset = {x: 0, y: 0};
    obj.w -= 3;
    obj.h -= 3;
    
    var canvas2=document.createElement("canvas");
    canvas2.width=obj.w;
    canvas2.height=obj.h
    var ctx2=canvas2.getContext("2d");
    
    ctx2.putImageData(ctx.getImageData(
      obj.pos[0].x+2,
      obj.pos[0].y+2, 
      obj.w,
      obj.h), 0, 0);
    
    obj.data = canvas2;
}
```

#### Move

##### 滑鼠點擊(Mousedown)
如果滑鼠點擊在範圍外，回到 Select 模式
否則將 drag 設為 true 供 Mousemove 使用
並且記錄當前的滑鼠座標

```javascript=
var mousePos = getMousePos(_canvas, evt);
var obj = components[components.length-1];
if (mousePos.x < obj.pos[0].x+obj.offset.x ||
    mousePos.x > obj.pos[0].x+obj.offset.x+obj.w ||
    mousePos.y < obj.pos[0].y+obj.offset.y ||
    mousePos.y > obj.pos[0].y+obj.offset.y+obj.h){
    changeMode('cut');
    return;
}
drag = true;
tempPoint.push(mousePos);
```

##### 滑鼠移動(Mousemove)
依照滑鼠的位置改變游標
如果正在拖曳，改變 clip 物件的 offset 達到移動的效果
並且將當前的滑鼠座標記錄下來

```javascript=
var mousePos = getMousePos(_canvas, evt);
var obj = components[components.length-1];

if (mousePos.x < obj.pos[0].x+obj.offset.x ||
    mousePos.x > obj.pos[0].x+obj.offset.x+obj.w ||
    mousePos.y < obj.pos[0].y+obj.offset.y ||
    mousePos.y > obj.pos[0].y+obj.offset.y+obj.h){
    _canvas.style.cursor = "default";
} else {
    _canvas.style.cursor = "move";
}

if(drag){
    obj.offset.x += mousePos.x - tempPoint[0].x;
    obj.offset.y += mousePos.y - tempPoint[0].y;
    
    tempPoint.pop();
    tempPoint.push(mousePos);
    
    redrawall();
}
```

##### 滑鼠跳起(Mouseup)
取消拖曳狀態
```javascript=
drag = false;
```

##### 重新繪製
首先會將原本位置的區域清除
接著將用以暫存的 canvas 塗入目的地
```javascript=
ctx.clearRect(comp.pos[0].x, comp.pos[0].y, comp.w, comp.h);
ctx.globalCompositeOperation="source-over";
ctx.drawImage(comp.data, 
              comp.pos[0].x+comp.offset.x, 
              comp.pos[0].y+comp.offset.y);
```
如果還在選取此區域，繪製外層的虛線長方形
```javascript=
ctx.setLineDash([3, 5]);

ctx.moveTo(comp.pos[0].x+comp.offset.x, comp.pos[0].y+comp.offset.y);

ctx.lineTo(comp.pos[0].x+comp.offset.x+comp.w,comp.pos[0].y+comp.offset.y);
ctx.lineTo(comp.pos[0].x+comp.offset.x+comp.w,
           comp.pos[0].y+comp.offset.y+comp.h);
ctx.lineTo(comp.pos[0].x+comp.offset.x,comp.pos[0].y+comp.offset.y+comp.h);
ctx.lineTo(comp.pos[0].x+comp.offset.x, comp.pos[0].y+comp.offset.y);

ctx.stroke();
ctx.setLineDash([]);
```

> 為甚麼不單純採用 getImageData/putImageData 方案？
> 
> 因為 PutImageData 無法將透明度資料疊加於原圖上(會直接覆蓋)，
> 導致如果原選取區域帶有透明部分，該部分將會顯示透明而非底下的原圖。
> 
>因此，這裡採取用drawImage的方式來達成期望的成果

### Gitlab page link

https://107062320.gitlab.io/AS_01_WebCanvas/

### Others

Library 使用

- Jquery
- FontAwesome
    - 提供本次作業所用的icon
    - fontawesome.js 提供框架，將 html 內描述正確的 i tag 轉換成相對應的 svg
    - regular.js/solid.min.js 描述轉換後的 svg
    - [更多詳情](https://www.w3schools.com/icons/fontawesome5_intro.asp)

<style>
table th{
    width: 100%;
}
</style>